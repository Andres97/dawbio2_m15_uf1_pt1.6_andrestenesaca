<%-- 
    Document   : pacients
    Created on : 29-ene-2019, 17:12:32
    Author     : alumne
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="model.Pacient"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>Pacients</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        
        <table border="1">
            <caption>Pacients list</caption>
            <tr>
                <th>Grup edad</th>
                <th>Peso</th>
                <th>IMC</th>
                <th>Clasificacion</th>
            </tr>
            <%
                 if(session.getAttribute("name")==null){
                        if(request.getAttribute("mid_list")!=null){
                            List<Pacient> pacient_list = (List<Pacient>)request.getAttribute("mid_list");
                            Iterator<Pacient> pacient_it = pacient_list.iterator();
                            while(pacient_it.hasNext()){
                                Pacient p = pacient_it.next();
                                out.println("<tr><td>"+p.getGrupEdat()+"</td><td>"+p.getPes());
                                out.println("</td><td>"+p.getImc()+"</td><td>"+p.getClasificacion()+"</td></tr>");
                            }
                        }
                 }
            %>
        </table>
    </body>
</html>
