<%-- 
    Document   : filtrar
    Created on : 05-feb-2019, 16:37:55
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>Filter Page</title>
    </head>
    <body> 
        <%@include file="menu.jsp" %>
        <form method="post" action="PatientController">
            
            <h3> Search Patient</h3>
            
            <label>Classificacion: </label>
            <select name="classificacio">
                <option value="OSTEOPENIA">Osteopenia</option>
                <option value="NORMAL">Normal</option>
                <option value="OSTEOPOROSI">Osteoporosi</option>
            </select>
            <br/>
            
            <br/>
            
            <label>Menopausia: </label>
            <br/>
            <input type="radio" name="menopausia" value="SI"> Si<br>
            <input type="radio" name="menopausia" value="NO" checked> No<br>
            <br/>
            
            <label>Tipo Menopausia: </label>
            <br/>
            <select name="tipusMenopausia">
                <option value="NO CONSTA">No consta</option>
                <option value="NATURAL">Natural</option>
                <option value="OVARIECTOMIA">Ovariectomia</option>
                <option value="HISTEROCTOMIa">Histeroctomia</option>
                <option value="AMBAS">Ambas</option>
            </select>
            
            <br/>
            <br/>
            <input type="submit" name="action" value="Search">
            <input type="submit" name="action" value="Back">
         </form>
        <%
            if(request.getParameter("error")!=null){
               String error=request.getParameter("error");
               switch (error) {
                       case "1":
                           out.println("No hay coincidencias");
                           break;
                       case "2":
                           out.println("No hay coincidencias para exportar");
                           break;
                   }
           }
        %>
    </body>
</html>
