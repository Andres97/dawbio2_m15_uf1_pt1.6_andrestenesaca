/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/** Model class patient
 * @author Andres Tenesaca Burgos
 */
public class Pacient {
    //atributos
    private int idRegister;
    private int edat;
    private String grupEdat;
    private int pes;
    private int talla;
    private double imc;
    private String clasificacion;
    private int menarquia;
    private String menopausia;
    private String tipusMenopausia;

    public Pacient() {
    }
    
    //Constructor que solo muestra una parte de la informacion
    public Pacient(String grupEdat, int pes, double imc, String clasificacion) {
        this.grupEdat = grupEdat;
        this.pes = pes;
        this.imc = imc;
        this.clasificacion = clasificacion;
    }
    
    //Constructor mostrar pacientes in ID
    public Pacient(int edat, String grupEdat, int pes, int talla, double imc, String clasificacion, int menarquia, String menopausia, String tipusMenopausia) {
        this.edat = edat;
        this.grupEdat = grupEdat;
        this.pes = pes;
        this.talla = talla;
        this.imc = imc;
        this.clasificacion = clasificacion;
        this.menarquia = menarquia;
        this.menopausia = menopausia;
        this.tipusMenopausia = tipusMenopausia;
    }
    
    //Constructo añadir paciente
    public Pacient(int idRegister, int edat, String grupEdat, int pes, int talla, String clasificacion, int menarquia, String menopausia, String tipusMenopausia) {
        this.idRegister = idRegister;
        this.edat = edat;
        this.grupEdat = grupEdat;
        this.pes = pes;
        this.talla = talla;
        double tallaD = this.talla/100.00;
        this.imc = this.pes/Math.pow(tallaD, 2);
        this.clasificacion = clasificacion;
        this.menarquia = menarquia;
        this.menopausia = menopausia;
        this.tipusMenopausia = tipusMenopausia;
    }
    
    
    //Getter's & Setter's
    public int getIdRegister() {
        return idRegister;
    }

    public void setIdRegister(int idRegister) {
        this.idRegister = idRegister;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    public String getGrupEdat() {
        return grupEdat;
    }

    public void setGrupEdat(String grupEdat) {
        this.grupEdat = grupEdat;
    }

    public int getPes() {
        return pes;
    }

    public void setPes(int pes) {
        this.pes = pes;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public double getImc() {
        return imc;
    }

    public void setImc(double imc) {
        this.imc = imc;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public int getMenarquia() {
        return menarquia;
    }

    public void setMenarquia(int menarquia) {
        this.menarquia = menarquia;
    }

    public String getMenopausia() {
        return menopausia;
    }

    public void setMenopausia(String menopausia) {
        this.menopausia = menopausia;
    }

    public String getTipusMenopausia() {
        return tipusMenopausia;
    }
    public void setTipusMenopausia(String tipusMenopausia) {
        this.tipusMenopausia = tipusMenopausia;
    }

    @Override
    public String toString() {
        //cambiar to string
        return idRegister + ";" + edat + ";" + grupEdat
                +";" + pes + ";" + talla + ";" + String.format("%.2f", imc) + ";" + clasificacion +
                ";" + menarquia + ";" + menopausia + ";" + tipusMenopausia+"\n";
    }
    
}
