/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**Model class user
 * @author Andres Tenesaca Burgos
 */
public class User {
    //atributos
    private String username;
    private String password;
    private String rol;

    //Contructors
    public User() {
    }
    
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public User(String username) {
        this.username = username;
    }
    
    //Getter's & Setter's
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
    //to_String method

    @Override
    public String toString() {
        return "User: " + "username= " + username + ", password= " + password + ", rol= " + rol;
    }
    
}
