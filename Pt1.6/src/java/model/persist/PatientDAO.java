package model.persist;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import model.Pacient;

/**
 * Archivo que se encarga de las diferentes queries SQL relacionado
 * con los pacientes
 * @author Andrés Tenesaca Burgos
 */

public class PatientDAO {

    private final Properties queries;
    private static String PROPS_FILE;
    private static DBConnect dataSource;

    public PatientDAO(String ruta) throws IOException {
        queries = new Properties();
        PROPS_FILE = ruta + "/patient_queries.properties";
        queries.load(new FileInputStream(PROPS_FILE));

        dataSource = DBConnect.getInstance();
    }

    public String getQuery(String queryName) {
        return queries.getProperty(queryName);
    }

    /**
     * <strong>getDataSource()</strong>
     *
     * @return object to connect to database.
     */
    public static DBConnect getDataSource() {
        return dataSource;
    }
    
    /**
     * Lista una pequeña informacion de los pacientes
     * @return array list de pacientes
     */
    public ArrayList<Pacient> list() {
        ArrayList<Pacient> list = new ArrayList<>();
        
        try ( Connection conn = dataSource.getConnection(); //creamos la conexion
              Statement st = conn.createStatement(); )
        {   
            //seleccionamos la query del archivo
            ResultSet res = st.executeQuery(getQuery("LIST"));
            
            //si ha encontrado alguna coincidencia
            while (res.next()) {
                Pacient pat = new Pacient(); //crear paciente
                
                //asignarle valores
                pat.setGrupEdat(res.getString("grupEdat"));
                pat.setPes(res.getInt("pes"));
                pat.setImc(res.getDouble("imc"));
                pat.setClasificacion(res.getString("classificació"));
                
                //añadir a la lista
                list.add(pat);
            }

        } catch (SQLException e) {
            list = new ArrayList<>();
        }
        
        return list;
    }
    
    /**
     * Lista todos los pacientes que coincidan con los datos que les pasa
     * @param menopausia string
     * @param tipusMenopausia string
     * @param classificacion string
     * @return ArrayList
     */
    public ArrayList<Pacient> searchPatient(String menopausia,
            String tipusMenopausia, String classificacion) {
        ArrayList<Pacient> list = new ArrayList<>();
        
        try ( Connection conn = dataSource.getConnection(); //creamos la conexion
                //preparamos la query
                PreparedStatement pst = conn.prepareStatement(getQuery("SEARCH"));)
        {
            pst.setString(1, classificacion);
            pst.setString(2, menopausia);
            pst.setString(3, tipusMenopausia);
            
            ResultSet rs = pst.executeQuery();
            
            //si ha encontrado alguna coincidencia
            while(rs.next()){
                Pacient p = new Pacient();
                
                //asignarle valores
                p.setEdat(rs.getInt("edat"));
                p.setGrupEdat(rs.getString("grupEdat"));
                p.setPes(rs.getInt("pes"));
                p.setTalla(rs.getInt("talla"));
                p.setImc(rs.getDouble("imc"));
                p.setClasificacion(rs.getString("classificació"));
                p.setMenarquia(rs.getInt("menarquia"));
                p.setMenopausia(rs.getString("menopausia"));
                p.setTipusMenopausia(rs.getString("tipusMenopausia"));
                
                //añadir a la lista
                list.add(p);
            }

        } catch (SQLException e) {
            list = new ArrayList<>();
        }
        
        return list;
    }
    
    /**
     * Lista toda la informacion de todos los pacientes
     * @return ArrayList
     */
    public List<Pacient> list_all() {
        ArrayList<Pacient> list = new ArrayList<>();
        
//crear conexion
        try ( Connection conn = dataSource.getConnection();
              Statement st = conn.createStatement(); )
        {   
            //preparar SQL
            ResultSet res = st.executeQuery(getQuery("LIST_ALL"));
            
            //mientras haya coincidencias
            while (res.next()) {
                Pacient pat = new Pacient();
                
                //asignar valores
                pat.setEdat(res.getInt("edat"));
                pat.setGrupEdat(res.getString("grupEdat"));
                pat.setPes(res.getInt("pes"));
                pat.setTalla(res.getInt("talla"));
                pat.setImc(res.getDouble("imc"));
                pat.setClasificacion(res.getString("classificació"));
                pat.setMenarquia(res.getInt("menarquia"));
                pat.setMenopausia(res.getString("menopausia"));
                pat.setTipusMenopausia(res.getString("tipusMenopausia"));
                
                //añadir a la lista
                list.add(pat);
            }

        } catch (SQLException e) {
            list = new ArrayList<>();
        }
        
        return list;
    }
    
    /**
     * Añade un paciente a la BBSS
     * @param p Pacient Obj
     * @return int
     */
    public int add(Pacient p) {
        int rowsAffected = 0;
        
        //crear conexion
        try ( Connection conn = dataSource.getConnection();
              PreparedStatement pst = conn.prepareStatement(getQuery("INSERT")); )
        {
            //asignar valores a la query
            pst.setInt(1, p.getIdRegister());
            pst.setInt(2, p.getEdat());
            pst.setString(3, p.getGrupEdat());
            pst.setInt(4, p.getPes());
            pst.setInt(5, p.getTalla());
            pst.setInt(6, (int) p.getImc());
            pst.setString(7, p.getClasificacion());
            pst.setInt(8, p.getMenarquia());
            pst.setString(9, p.getMenopausia());
            pst.setString(10, p.getTipusMenopausia());
            
            //devulve el numero de columnas añadidas, es decir, 1
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }
        
        return rowsAffected;
    }
}
