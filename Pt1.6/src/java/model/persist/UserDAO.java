/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.persist;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import model.User;

/**
 * Archivo que se encarga de las diferentes queries SQL relacionado
 * con los pacientes
 * @author Andrés Tenesaca Burgos
 */
public class UserDAO {
    private final Properties queries;
    private static String PROPS_FILE;
    private static DBConnect dataSource;
    
    public UserDAO(String ruta) throws IOException {
        queries = new Properties();
        PROPS_FILE = ruta + "/users_queries.properties";
        queries.load(new FileInputStream(PROPS_FILE));

        dataSource = DBConnect.getInstance();
    }

    public String getQuery(String queryName) {
        return queries.getProperty(queryName);
    }

    /**
     * <strong>getDataSource()</strong>
     *
     * @return object to connect to database.
     */
    public static DBConnect getDataSource() {
        return dataSource;
    }
    
    /**
     * añade un usuario a la base de datos mediante una SQL preparada
     * @param u User obj
     * @return String rol
     */
    public String findOne(User u) {
        String rol = null;
        
        //crea conexion
        try ( Connection conn = dataSource.getConnection();
              PreparedStatement pst = conn.prepareStatement(getQuery("FIND_ONE"));)
        {
            //añade valor a los datos de la query
            pst.setString(1, u.getUsername());
            pst.setString(2, u.getPassword());
            ResultSet rs = pst.executeQuery();
            
            //mientras haya coincidencias
            while(rs.next()){
                //asignar rol al user obj
                u.setRol(rs.getString("rol"));
            }
            
            rol = u.getRol();
            
        } catch (SQLException e) {
            rol = null;
        }
        
        return rol;
    }
    
    /**
     * Busca un usuario de la base de datos
     * @param username
     * @return ArrayList of users
     */
    public ArrayList<User> search_user(String username) {
        ArrayList<User> userList = new ArrayList();
        
        //crear conexion
        try ( Connection conn = dataSource.getConnection();
              PreparedStatement pst = conn.prepareStatement(getQuery("SEARCH_USER"));)
        {
            //asignar valor a la query
            pst.setString(1, username);
            ResultSet rs = pst.executeQuery();
            
            //mientras haya coincidencias
            while(rs.next()){
                User u = new User();
                //asignar el nombre al user obj
                u.setUsername(rs.getString("nom"));
            }
            
            
        } catch (SQLException e) {
            userList = new ArrayList<>();
        }
        
        return userList;
    }
    
    /**
     * Funcion que se encarga de añadir un usuario a la base de datos
     * @param u User obj
     * @return 
     */
    public int add_user(User u) {
        int rowsAffected = 0;
        
        //crear conexion
        try ( Connection conn = dataSource.getConnection();
              PreparedStatement pst = conn.prepareStatement(getQuery("INSERT"));)
        {
            //asignar valor a la query
            pst.setString(1, u.getUsername());
            pst.setString(2, u.getPassword());
            pst.setString(3, "basic");
            
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }
        
        return rowsAffected;
    }
    
    
}
