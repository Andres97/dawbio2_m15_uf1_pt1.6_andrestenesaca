/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Pacient;
import model.User;
import model.persist.PatientDAO;
import model.persist.UserDAO;

/**Archivo que se encarga de gestionar las diferentes opciones relacionadas con
 * los usuarios
 * @author Andrés Tenesaca Burgos
 * @version 1.0
 */
@WebServlet(name = "UserController", urlPatterns = {"/UserController"})
public class UserController extends HttpServlet {
    //variables de los modelos
    private String ruta;
    private UserDAO uDAO;
    private PatientDAO pDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //ruta de los archivos
        ruta = getServletContext().getRealPath("/WEB-INF/resources");
        //Objects DAO
        uDAO = new UserDAO(ruta);
        pDAO = new PatientDAO(ruta);
        
        //comprobacion de valor nulo
        if(request.getParameter("action")!=null){
            //guardamos el valor en una variable
            String action=request.getParameter("action");
            //dependiendo del valor hara una cosa u otra
            switch(action){
                case "Login":
                    login(request,response); //funcion login
                    break;
                case "Add":
                    add_user(request, response);
                    break;
            }
        }
        else {
            //si es null te devuelve a la pagina principal
            response.sendRedirect("index.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /**
     * Funcion que se encarga de verificar el login de los usuarios
     * y mostrar errores respecto a estos
     * @param request
     * @param response
     * @throws IOException 
     */
    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //variables que contiene las credenciales de los usuarios
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        
        //crea un objeto usuario
        User u = new User(username, password);
        
        //si el campo usuario o contraseña estan vacios, error
        if(username.isEmpty() || password.isEmpty()){
            response.sendRedirect("login.jsp?error=2");
        }
        //llama a la funcion findone del UserDAO
        //para ver si el usuario existe
        else if(uDAO.findOne(u) != null){
            //crea una variable de session con el nombre del usuario
            HttpSession session=request.getSession();
            session.setAttribute("name", username);
            //crea una lista con los datos de todos los pacientes
            List<Pacient> all = pDAO.list_all();
            
            session.setAttribute("rol", uDAO.findOne(u));
            
            request.setAttribute("full_list", all);
            RequestDispatcher d=request.getRequestDispatcher("home.jsp");
            d.forward(request, response);
        }
        //si el usuario no existe muestra un error
        else {
            response.sendRedirect("login.jsp?error=1");
        }
    }
    
    /**
     * Funcion que se encarga de añadir un nuevo usuario e informar de los
     * posibles errores que cometa el usuario a la hora de añadirlo
     * @param request
     * @param response
     * @throws IOException 
     */
    private void add_user(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter("username");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        
        //crear nuevo user obj
        User u = new User(username, password1);
        
        //error si los campos estan vacios
        if(username.isEmpty() || password1.isEmpty() || password2.isEmpty()){
            response.sendRedirect("add_user.jsp?error=1");
        }
        //error si las contraseñas no coinciden
        else if(!password1.equals(password2)){
            response.sendRedirect("add_user.jsp?error=2");
        }
        //error si el nombre de usuario ya existe
        else if(!uDAO.search_user(username).isEmpty()){
            response.sendRedirect("add_user.jsp?error=3");
        }
        //mensaje si se ha añadido el usuario
        else if(uDAO.add_user(u)==1){
            response.sendRedirect("add_user.jsp?error=4");
        }
    }
}
