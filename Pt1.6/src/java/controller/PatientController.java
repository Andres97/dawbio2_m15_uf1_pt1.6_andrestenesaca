/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Pacient;
import model.persist.PatientDAO;

/** Archivo que se encarga de gestionar las diferentes opciones que esten
 * relacionadas con los pacientes
 * @author Andrés Tenesaca Burgos
 * @version 1.0
 */
@WebServlet(name = "Controller", urlPatterns = {"/PatientController"})
public class PatientController extends HttpServlet { 
    private String ruta;
    private PatientDAO pDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //ruta de los archivos        
        ruta = getServletContext().getRealPath("/WEB-INF/resources");
        
        //comprobacion de valor nulo
        if(request.getParameter("action")!=null){
            //guardamos el valor en una variable
            String action=request.getParameter("action");
            //dependiendo del valor hara una cosa u otra
            switch(action){
                case "Back": //volver atras
                    List<Pacient> all = pDAO.list_all();
                    request.setAttribute("full_list", all);
                    RequestDispatcher d=request.getRequestDispatcher("home.jsp");
                    d.forward(request, response);
                    break;
                case "Add Pacient": //añadir un paciente
                    add_patient(request, response);
                    break;
                case "Search":
                    search_patient(request, response);
                    break;
            }
        }
        else {
            //si es null llama a la fucion la cual te redirije a la pagia principal
            list_pacients(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /**
     * Funcion que se encarga de devolver una pequeña parte del archivo
     * osteoporosis en una list y se la envia a otro archivo
     * @param request
     * @param response
     * @throws IOException 
     */
    private void list_pacients(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //lista que contiene algunos datos de los pacientes
        
        pDAO = new PatientDAO(ruta);
        ArrayList<Pacient> pats = pDAO.list();
        
        request.setAttribute("mid_list", pats);
        RequestDispatcher d=request.getRequestDispatcher("pacients.jsp");
        d.forward(request, response);
    }
    
    /***
     * Funcion que se encarga de buscar pacientes por menopausia
     * @param request
     * @param response
     * @throws IOException 
     */
    private void search_patient(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //guardar daots en variables
        String menopausia = request.getParameter("menopausia");
        String tipusMenopausia = request.getParameter("tipusMenopausia");
        String classificacion = request.getParameter("classificacio");
        
        //crear objeto PatienDAO
        pDAO = new PatientDAO(ruta);
        //guardamos en un arrayList los select del filtrar
        ArrayList<Pacient> search_patients = pDAO.searchPatient(menopausia,tipusMenopausia,classificacion);
        
        //si el ArrayList esta vacio muestra un error
        if(search_patients.isEmpty()){
            response.sendRedirect("filtrar.jsp?error=1");
        }
        else {
            //en caso contrario muestra los resultado en un tabla
            request.setAttribute("search_list", search_patients);
            RequestDispatcher d=request.getRequestDispatcher("filtrar_list.jsp");
            d.forward(request, response);                
        }
    }
    
    /**
     * Funcion que se encarga de verificar si los datos introducidos
     * del paciente son correctos y entonces lo añade
     * @param request
     * @param response
     * @throws IOException 
     */
    private void add_patient(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //lsta que contiene todos los datos de los pacientes
        List<Pacient> all = pDAO.list_all();
        
        //zona de variables
        int idRegistre = all.size()+1; //coge el tamaño de la lsta actual y suma 1
        String edad = request.getParameter("edad");
        String grupEdad = request.getParameter("grupEdad");
        String peso = request.getParameter("peso");
        String talla = request.getParameter("talla");
        String clasificacion = request.getParameter("clasificacion");
        String menarquia = request.getParameter("menarquia");
        String menopausia = request.getParameter("menopausia");
        String tipoMenopausia = request.getParameter("tipoMenopausia");
        
        //si alguna de los siguientes campos estan vacios devuelve un error
        if(edad.isEmpty() || grupEdad.isEmpty() || peso.isEmpty() || 
                talla.isEmpty() || menarquia.isEmpty()){
            //redirije con el error
            response.sendRedirect("add_pacient.jsp?error=2");
        }
        //si algunos de los campos numericos es menor a 0, error
        else if(Integer.parseInt(peso) <0 || Integer.parseInt(talla) < 0 || 
                Integer.parseInt(menarquia)<0){
            response.sendRedirect("add_pacient.jsp?error=1");
        }
        //si la edad es memor a 45, error
        //es menor a 45 porque en el select de rango edad
        //no hay ninguna edad menor a 45
        else if(Integer.parseInt(edad)<45){
            response.sendRedirect("add_pacient.jsp?error=4");
        }
        /*
        Error que comprueba si el usuario ha introducido una edad
        superior a 100
        */
        else if(Integer.parseInt(edad)>=100){
            response.sendRedirect("add_pacient.jsp?error=5");
        }
        //si no ha entrado en ninguno de los campos anteriores
        //añade el paciente
        else {
            //crea un nuevo paciente con las variables anteriores
            Pacient p = new Pacient(idRegistre, Integer.parseInt(edad), grupEdad, 
            Integer.parseInt(peso), Integer.parseInt(talla), clasificacion,
            Integer.parseInt(menarquia), menopausia, tipoMenopausia);
            
            //llama a la funcion añadir ded PacientDAO
            //si lo ha añadido devuelve 1
            if(pDAO.add(p)==1){
                //vuevle a actualizar la lista con el nuevo paciente añadido
                all = pDAO.list_all();
                
                request.setAttribute("full_list", all);
                RequestDispatcher d=request.getRequestDispatcher("home.jsp");
                d.forward(request, response);
            }
            //en caso contrario muestra un error
            else {
                response.sendRedirect("add_pacient.jsp?error=3");
            }
        }
        
    }
}