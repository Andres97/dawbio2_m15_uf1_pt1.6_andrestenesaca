<%-- 
    Document   : login
    Created on : 30-ene-2019, 16:59:50
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>Login Page</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <h3>Log in user</h3>
        <form method="post" action="UserController">
            Usuario: <input type="text" name="username"/>
            <br/>
            Contraseña: <input type="password" name="password" />
            <br/>
            <input type="submit" name="action" value="Login"/>
        </form>
        <%
            
           if(request.getParameter("error")!=null){
               String error=request.getParameter("error");
               switch (error) {
                       case "1":
                           out.println("Usuario y/o contraseña incorrectas");
                           break;
                       case "2":
                           out.println("Usuario y/o contraseña vacio");
                           break;
                   }
           }
        %>
    </body>
</html>
