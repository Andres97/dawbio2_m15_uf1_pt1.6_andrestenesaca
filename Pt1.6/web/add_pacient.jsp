<%-- 
    Document   : add_pacient
    Created on : 05-feb-2019, 16:45:55
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>Add Pacient Page</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <h3>Add Pacient</h3>
        <form method="post" action="PatientController">
            <label>Edad: </label>
            <input type="number" name="edad" min="0"/>
            <br/>
            
            <label>Grup Edad</label>
            <select name="grupEdad">
                <option value="fins a 45">fins a 45</option>
                <option value="45 - 49">45 - 49</option>
                <option value="50 - 54">50 - 54</option>
                <option value="55 - 59">55 - 59</option>
                <option value="60 - 64">60 - 64</option>
                <option value="65 - 69">65 - 69</option>
                <option value="mes de 69">mes de 69</option>
            </select>
            
            <br>
            <label>Peso (kg)</label>
            <input type="number" name="peso" min="0">
            
            <br>
            <label>Talla (cm)</label>
            <input type="number" name="talla" min="0">
            
            <br>
            <label>Clasificacion</label>
            <select name="clasificacion">
                <option value="OSTEOPENIA">Osteopenia</option>
                <option value="NORMAL">Normal</option>
                <option value="OSTEOPOROSI">Osteoporosi</option>
            </select>
            
            <br>
            <label>Menarquia</label>
            <input type="number" name="menarquia" min="0">
            
            <br>
            <label>Menopausia</label>
            <input type="radio" name="menopausia" value="SI"> Si
            <input type="radio" name="menopausia" value="NO" checked="checked"> No
            
            <br>
            <label>Tipo Menopausia</label>
            <select name="tipoMenopausia">
                <option value="NO CONSTA">No consta</option>
                <option value="OSTEOPENIA">Osteopenia</option>
                <option value="NATURAL">natural</option>
                <option value="OVARIECTOMIA">Ovariectomia</option>
                <option value="HISTEROCTOMIA">Histeroctomia</option>
                <option value="AMBAS">Ambas</option>
            </select>
            
            <br><br>
            <input type="submit" name="action" value="Add Pacient"/>
            <input type="submit" name="action" value="Back">
        </form>
        <%
            
           if(request.getParameter("error")!=null){
               String error=request.getParameter("error");
               switch (error) {
                       case "1":
                           out.println("Los campos numericos deben de ser mayor a 0");
                           break;
                       case "2":
                           out.println("Debes rellenar todos los campos");
                           break;
                       case "3":
                           out.println("Error al añadir el paciente");
                           break;
                       case "4":
                           out.println("La edad tiene que ser mayor a 45");
                           break;
                       case "5":
                           out.println("La edad tiene que ser menor a 100");
                   }
           }
        %>
    </body>
</html>
