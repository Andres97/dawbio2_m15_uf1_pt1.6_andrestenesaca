<%-- 
    Document   : home
    Created on : 30-ene-2019, 17:32:58
    Author     : alumne
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="model.Pacient"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>Home Page</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <%
            if(session.getAttribute("name")==null){
                response.sendRedirect("index.jsp");
            }
            else {
                out.println("<h3> Hello " + session.getAttribute("name")+"</h3>");
            }
        %>
        <table>
            <caption>Patients list</caption>
            <tr>
                <th>Edad</th>
                <th>Grupo edad</th>
                <th>Pes</th>
                <th>Talla</th>
                <th>IMC</th>
                <th>Clasificacion</th>
                <th>Menarquia</th>
                <th>Menopausia</th>
                <th>Tipo menopausia</th>
            </tr>
            <%
                if(request.getAttribute("full_list")!=null){
                    List<Pacient> pacient_list = (List<Pacient>)request.getAttribute("full_list");
                    Iterator<Pacient> pacient_it = pacient_list.iterator();
                    while(pacient_it.hasNext()){
                        Pacient p = pacient_it.next();
                        out.println("<tr><td>"+p.getEdat()+"</td>"
                                + "<td>"+p.getGrupEdat()+"</td><td>"+p.getPes()+"</td>"
                                + "<td>"+p.getTalla()+"</td><td>"+p.getImc()+"</td>"
                                + "<td>"+p.getClasificacion()+"</td><td>"+p.getMenarquia()+"</td>"
                                + "<td>"+p.getMenopausia()+"</td><td>"+p.getTipusMenopausia()+"</td>");
                    }
                }
                else {
                    out.println("Error al cargar el archivo");
                }
            %>
        </table>
    </body>
</html>
