<%-- 
    Document   : add_user
    Created on : 19/03/2019, 17:02:07
    Author     : mati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>Add user page</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <h3>Add user</h3>
        
        <form method="post" action="UserController">
            <label>Nombre: </label>
            <input type="text" name="username">
            <br/>
            
            <label>Password: </label>
            <input type="password" name="password1">
            <br/>
            
            <label> Repeat password: </label>
            <input type="password" name="password2">
            <br/>
            
            <br/><br/>
            <input type="submit" name="action" value="Add"/>
        </form>
        <%
            
           if(request.getParameter("error")!=null){
               String error=request.getParameter("error");
               switch (error) {
                       case "1":
                           out.println("Usuario y/o contraseña vacio");
                           break;
                       case "2":
                           out.println("Las contraseñas no coinciden");
                           break;
                       case "3":
                           out.println("Elija otro nombre de usuario");
                           break;
                       case "4":
                           out.println("Usuario añadido");
                           break;
                   }
           }
        %>
    </body>
</html>
