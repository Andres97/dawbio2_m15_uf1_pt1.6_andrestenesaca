<%-- 
    Document   : filtrar_list
    Created on : 20/03/2019, 16:51:00
    Author     : mati
--%>

<%@page import="java.util.Iterator"%>
<%@page import="model.Pacient"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./index.css" rel="stylesheet" type="text/css">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <table>
            <caption>Search list</caption>
            <tr>
                <th>Edad</th>
                <th>Grupo edad</th>
                <th>Pes</th>
                <th>Talla</th>
                <th>IMC</th>
                <th>Clasificacion</th>
                <th>Menarquia</th>
                <th>Menopausia</th>
                <th>Tipo menopausia</th>
            </tr>
            <%
                if(request.getAttribute("search_list")!=null){
                    List<Pacient> search_list = (List<Pacient>)request.getAttribute("search_list");
                    Iterator<Pacient> pacient_it = search_list.iterator();
                    while(pacient_it.hasNext()){
                        Pacient p = pacient_it.next();
                        out.println("<tr><td>"+p.getEdat()+"</td>"
                                + "<td>"+p.getGrupEdat()+"</td><td>"+p.getPes()+"</td>"
                                + "<td>"+p.getTalla()+"</td><td>"+p.getImc()+"</td>"
                                + "<td>"+p.getClasificacion()+"</td><td>"+p.getMenarquia()+"</td>"
                                + "<td>"+p.getMenopausia()+"</td><td>"+p.getTipusMenopausia()+"</td>");
                    }
                }
                else {
                    out.println("Error al cargar el archivo");
                }
            %>
        </table>
    </body>
</html>
